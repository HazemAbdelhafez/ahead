from filter import DataMotionFilter, ComputeFilter
from strings import BYTES_STR, GPU_DM_TIME_STR, SRC_KIND_STR, DST_KIND_STR, COPY_KIND_STR
from config import OLD_NODE, NEW_NODE, CORAL2_APPS
from models import DRAMModel, GPUMemoryModel, NvLinkModel, PCILinkModel

# PATHS
microbenchmarks_traces_path = "./%s_Data/Bandwidth_Benchmarking/"


class Utils:

    def __init__(self):
        pass

    @staticmethod
    def extract_data_transfers(app_trace_file_path):
        """ Reads CUPTI (nvvp) output file containing timing information for CUDA data copying,
        e.g. htod or dtoh or dtod"""

        _filter = DataMotionFilter(app_trace_file_path)
        _df = _filter.get_data_as_data_frame()
        return _df

    @staticmethod
    def extract_compute_kernels(app_trace_file_path):
        """ Reads CUPTI (nvvp) output file containing timing information for CUDA kernels """
        _filter = ComputeFilter(app_trace_file_path)
        _df = _filter.get_data_as_data_frame()
        return _df

    @staticmethod
    def read_microbenchmark(_microbenchmark_trace_file_path):
        """ Reads CUPTI (nvvp) output file containing timing information for CUDA data copying unidirectional,
        e.g. htod or dtoh or dtod"""

        # Read the NVPROF output file (nvvp format)
        _filter = DataMotionFilter(_microbenchmark_trace_file_path)
        _df = _filter.get_data_as_data_frame()

        # Remove outliers from group intervals (grouping key is the bytes)
        low = 0.003
        high = 0.977
        _df = _df[_df.groupby([BYTES_STR])[GPU_DM_TIME_STR].transform(lambda x: (x <= x.quantile(high)) &
                                                                                (x >= x.quantile(low))).eq(1)]
        # Calculate the mean of the transfer time grouped by Bytes key
        _df = _df.groupby([BYTES_STR], as_index=False).mean()
        _df[SRC_KIND_STR] = _df[SRC_KIND_STR].astype(int)
        _df[COPY_KIND_STR] = _df[COPY_KIND_STR].astype(int)
        _df[DST_KIND_STR] = _df[DST_KIND_STR].astype(int)
        return _df

    @staticmethod
    def set_node_configuration(_node):
        _applications = None
        _microbenchmark_files_path = None
        _dram_memory_model = None
        _gpu_memory_model = None
        _gpu_dram_interface_model = None

        if _node == NEW_NODE:
            _applications = CORAL2_APPS.copy()
            _dram_memory_model = DRAMModel(mem_freq=1333, mem_banks_per_cpu=8,
                                           mem_module_size=32, bus_width=64)
            _gpu_memory_model = GPUMemoryModel(bus_width=4096, mem_freq=876)
            _nvlink_interface_model = NvLinkModel()
            _gpu_dram_interface_model = _nvlink_interface_model
            _gpu_dram_interface_model.set_latency(0.0)

        elif _node == OLD_NODE:
            _applications = CORAL2_APPS.copy()
            _dram_memory_model = DRAMModel(mem_freq=800, mem_banks_per_cpu=2,
                                           mem_module_size=8, bus_width=64, num_cpus=1)
            _gpu_memory_model = GPUMemoryModel(bus_width=384, mem_freq=2600)
            _pci_interface_model = PCILinkModel(max_lane_bw=5, num_lanes=16,
                                                encoding_efficiency=80,
                                                mr_o=24, mw_o=24, cpd_o=20)
            _gpu_dram_interface_model = _pci_interface_model
            _gpu_dram_interface_model.set_latency(0.0)

        else:
            print("Invalid node.")
            exit(0)

        # We need the path of microbenchmark traces of the old node.
        _microbenchmark_files_path = microbenchmarks_traces_path % _node
        return _applications, _dram_memory_model, _gpu_memory_model, _gpu_dram_interface_model, \
            _microbenchmark_files_path
