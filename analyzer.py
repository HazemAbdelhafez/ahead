import pandas as pd
import sqlite3
import copy


class Analyzer:
    def __init__(self):
        pass

    @staticmethod
    def read_app_trace_file(_file, _query):
        connection = sqlite3.connect(_file)
        df = pd.read_sql_query(_query, connection)
        connection.close()
        return copy.deepcopy(df)

    @staticmethod
    def read_app_trace_files(_paths, _query):
        if type(_paths) is list:
            paths_iter = iter(_paths)
            _path = next(paths_iter)
            _df_merged = Analyzer.read_app_trace_file(_path, _query)
            for _path in paths_iter:
                _df = Analyzer.read_app_trace_file(_path, _query)
                _df_merged = pd.concat([_df_merged, _df], ignore_index=True)
        else:
            _df_merged = Analyzer.read_app_trace_file(_paths, _query)
        _data_frame = _df_merged
        return copy.deepcopy(_data_frame)

